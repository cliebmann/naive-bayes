import numpy as np
from scipy import io as spio

NEWS_DATA_FNAME = "../data/news.mat"
NEWS_VOCAB_FNAME = "../data/news.vocab"
PARAMS_FNAME = "../data/nb_params.mat"

# read news data

def read_news_data():
    """Subtracts 1 from label to 0 index"""
    news = spio.loadmat(NEWS_DATA_FNAME)
    return {
      'training': {
        'feature_vector': news['data'].astype(float),
        'labels': news['labels'] - 1,
      },
      'test': {
        'feature_vector': news['testdata'].astype(float),
        'labels': news['testlabels'] - 1,
      },
    }

def read_vocab():
    with open(NEWS_VOCAB_FNAME, 'r') as f:
        return [line for line in f]

# naive bayes

def count_groups(y):
    return np.bincount(y[:, 0])

def class_priors_mle(n, y):
    """Compute class priors with maximum likelihood estimate
    """
    return count_groups(y) / n

def class_conditionals_ls(x, y):
    """Compute class conditionals with laplace smoothing estimator
    """
    group_counts = count_groups(y)[:, np.newaxis]
    groups = np.unique(y)
    
    gw = []
    for g in range(np.max(groups) + 1):
        g_sum = np.zeros(x.shape[1])
        in_group = np.where(y == g)[0]
        for xx in x[in_group]:
            for i, xxx in enumerate(xx):
                g_sum[i] += xxx # {0, 1}
        gw.append(g_sum)
    
    agw = np.array(gw)
    
    num = 1 + agw
    denom = 2 + group_counts
    
    return num / denom

def nb_params(td):
    x = td['feature_vector']
    y = td['labels']
    n = np.shape(y)[0]
    
    pis = class_priors_mle(n, y)    
    mus = class_conditionals_ls(x.toarray(), y)

    return { 'pis': pis, 'mus': mus }

def alphas_logmus(params):
    pis = params['pis'][0][0] # pis.shape == (1,20)
    mus = params['mus'][0][0] # mus.shape == (20, 61188)

    logpis = np.log(pis) # log_pis.shape == (1,20)
    logmus = np.log(mus) # log_mus.shape == (20, 61188)

    return logpis, logmus

def nb_classify(params, test):
    # test.shape == (1, 61188)
    alphas, logmus = alphas_logmus(params)

    s = np.sum(logmus + test, axis=1) # s.shape == (20,)
    idx = np.argmax(alphas[0] + s)

    return idx

def test_nb_classify(params, tests):
    def error_rate(a, b):
        num_examples = np.shape(a)[0]
        return np.sum(a != b, axis=0)[0] / num_examples

    test_labels = tests['labels']
    labels = [nb_classify(params, test) for test in tests['feature_vector']]

    er = error_rate(test_labels, labels)
    print(er) # test/training error rate

    return labels

def largest_words(params, vocab):
    alphas, logmus = alphas_logmus(params)
    top_20_idx = np.argpartition(logmus, -20, axis=1)[:,-20:]
    top_20 = vocab[top_20_idx]
    print(top_20)

# drivers

def gen_nb_params():
    news = read_news_data()
    td = news['training']
    params = nb_params(td)
    spio.savemat(PARAMS_FNAME, {'params': params})

def training_error():
    news = read_news_data()
    params = spio.loadmat(PARAMS_FNAME)['params']
    test_nb_classify(params, news['training'])

def test_error():
    news = read_news_data()
    params = spio.loadmat(PARAMS_FNAME)['params']
    test_nb_classify(params, news['test'])

def run_largest_words():
    params = spio.loadmat(PARAMS_FNAME)['params']
    vocab = np.array(news_vocab())
    largest_words(params, vocab)

if __name__ == '__main__':
    test`_error()
