import numpy as np
from scipy import io as spio
from scipy import sparse

from naive_bayes import nb_params

NEWS_DATA_FNAME = "../data/news.mat"
NEWS_VOCAB_FNAME = "../data/news.vocab"
NEWS_GROUPS_FNAME = "../data/news.groups"

def read_news_data():
    """Subtracts 1 from label to 0 index"""
    news = spio.loadmat(NEWS_DATA_FNAME)
    return {
      'training': {
        'feature_vector': news['data'].astype(float),
        'labels': news['labels'] - 1,
      },
      'test': {
        'feature_vector': news['testdata'].astype(float),
        'labels': news['testlabels'] - 1,
      },
    }


def main():
    news = read_news_data()
    td = news['training']
    params = nb_params(td)
    spio.savemat('nb_params.mat', {'params': params})

if __name__ == '__main__':
    main()