from scipy import io as spio
import numpy as np

NEWS_DATA_FNAME = "../data/news.mat"
NEWS_VOCAB_FNAME = "../data/news.vocab"

def load_data():
    data = spio.loadmat(NEWS_DATA_FNAME)
    with open(NEWS_VOCAB_FNAME) as f:
        vocab = list(map(lambda x: x.strip(), f.readlines()))
    print('Loaded {} words in vocabulary'.format(len(vocab)))
    print('Loaded training data: {} vectors of length {}'.format(*data['data'].shape))
    print('Loaded test data: {} vectors of length {}'.format(*data['testdata'].shape))
    print('Found {} unique labels'.format(len(np.unique(data['labels']))))

    return data, vocab

def compute_prior_distribution(labels):
    # ignore the first bin because we have no class 0
    return np.bincount(labels.reshape(len(labels)))[1:] * 1.0 / len(labels)

def compute_class_conditional_parameters(data, labels):
    # use Laplace smoothing
    labels = labels.reshape(len(labels))
    unique_labels = np.unique(labels)

    mu = np.zeros((len(unique_labels), data.shape[1]))

    for (idx, y) in enumerate(unique_labels):
        data_slice = data[labels == y, :]
        mu[idx, :] = (1.0 + data_slice.sum(axis=0)) / (2 + data_slice.shape[0])

    return mu

def compute_log_probability(data, pi, mu):
    # output should be examples x classes
    data = np.matrix(data.toarray())

    log_mu = np.log(mu)
    log_1_minus_mu = np.log(1 - mu)

    ccd = data * log_mu.T + (1 - data) * log_1_minus_mu.T

    return ccd + np.log(pi)

def compute_label(log_prob):
    # need to offset by one to match the class labels
    return np.argmax(log_prob, axis=1) + 1

def compute_error_rate(correct_labels, predicted_labels):
    return ((predicted_labels != correct_labels).sum() * 1.0 /
            len(corrected_labels))

def compute_interesting_vocabulary_per_class(mu, vocab):

    for label in range(mu.shape[0]):
        print('Label {}\n======='.format(label + 1))
        alphas = list(map(lambda x: x[0], sorted(enumerate(mu[label, :]),
          key=lambda x: x[1])))
        map(print, map(lambda x: vocab[x], reversed(alphas[-20:])))
        print('\n')

if __name__ == '__main__':
    data, vocab = load_data()

    pi = compute_prior_distribution(data['labels'])
    mu = compute_class_conditional_parameters(data['data'], data['labels'])

    log_prob_train = compute_log_probability(data['data'], pi, mu)
    log_prob_test = compute_log_probability(data['testdata'], pi, mu)

    labels_train = compute_label(log_prob_train)
    labels_test = compute_label(log_prob_test)

    print('Train error rate: {}'.format(compute_error_rate(data['labels'],
                                                          labels_train)))
    print('Test error rate: {}'.format(compute_error_rate(data['testlabels'],
                                                          labels_test)))

    compute_interesting_vocabulary_per_class(mu, vocab)

