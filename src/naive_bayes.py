import numpy as np

def count_groups(y):
    return np.bincount(y[:, 0])

def class_priors_mle(n, y):
    """Compute class priors with maximum likelihood estimate
    """
    return count_groups(y) / n

def class_conditionals_ls(x, y):
    """Compute class conditionals with laplace smoothing estimator
    """
    group_counts = count_groups(y)[:, np.newaxis]
    groups = np.unique(y)
    
    gw = []
    for g in range(np.max(groups) + 1):
        g_sum = np.zeros(x.shape[1])
        in_group = np.where(y == g)[0]
        for xx in x[in_group]:
            for i, xxx in enumerate(xx):
                g_sum[i] += xxx # {0, 1}
        gw.append(g_sum)
    
    agw = np.array(gw)
    
    num = 1 + agw
    denom = 2 + group_counts
    
    return num / denom

def nb_params(td):
    x = td['feature_vector']
    y = td['labels']
    n = np.shape(y)[0]
    
    pis = class_priors_mle(n, y)    
    mus = class_conditionals_ls(x.toarray(), y)

    return { 'pis': pis, 'mus': mus }